# Table of Contents

* [Introduction](README.md)

## Meet the Team

* [BSIS 2](team/bsis_2.md)
* [ACT 2](team/act_2.md)
* [2022 BSIS 2](team/2022_bsis_2.md)
* [2022 ACT 2](team/2022_act_2.md)

## Tasks

* [Tasks](tasks/tasks.md)

## Exploring the subject

* [Links and Resources](subject_exploration/links_and_resources.md)
* [Notes](subject_exploration/notes.md)
